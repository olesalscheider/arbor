# Copyright 2012-2018 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ suffix=tar.gz ]

SUMMARY="C++ library and set of programs that inspect and manipulate the structure of PDF files"
DESCRIPTION="
QPDF is a C++ library and set of programs that inspect and manipulate the
structure of PDF files. It can encrypt and linearize files, expose the
internals of a PDF file, and do many other operations useful to end users and
PDF developers.
"

UPSTREAM_DOCUMENTATION="${HOMEPAGE}/files/${PN}-manual.html [[ lang = en ]]"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        sys-libs/zlib
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
    test:
        app-text/ghostscript[>=9.21-r4][tiff]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-os-secure-random
    --disable-doc-maintenance
    --disable-html-doc
    --disable-insecure-random
    --disable-pdf-doc
    --disable-static
    --disable-validate-doc
    --disable-werror
    --with-random=/dev/urandom
)

DEFAULT_SRC_CONFIGURE_TESTS=(
    '--enable-show-failed-test-output --disable-show-failed-test-output'
    '--enable-test-compare-images --disable-test-compare-images'
)

# TODO: Use src_test_expensive to run the largefiles test suite

